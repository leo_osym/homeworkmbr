﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkMBR
{
    public static class InitDictionary
    {
        public static Dictionary<byte, string> Init()
        {
            Dictionary<byte, string> partype = new Dictionary<byte, string> { };
            string line;
            using (StreamReader stream = new StreamReader(@"./ptypes.txt", Encoding.ASCII))
            {

                while ((line = stream.ReadLine()) != null)
                {
                    byte b = Byte.Parse(line.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                    string desc = line.Substring(3, line.Length - 3);
                    partype.Add(b, desc);
                }
            }

            return partype;
        }
    }
}
