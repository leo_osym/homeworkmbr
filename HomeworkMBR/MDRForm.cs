﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeworkMBR
{
    public partial class MDRForm : Form
    {
        public MDRForm()
        {
            InitializeComponent();
        }
        const uint GENERIC_READ = 0x80000000;
        const uint FILE_SHARE_READ = 0x1;
        const uint FILE_SHARE_WRITE = 0x2;
        const uint OPEN_EXISTING = 0x3;
        const uint FILE_ATTRIBUTE_NORMAL = 0x80;
        const uint ERROR_INSUFFICIENT_BUFFER = 0x7A;
        public byte[] mbr = new byte[512];// init buffer
        Dictionary<byte, string> partype = new Dictionary<byte, string>();//dictionary for partition types

        internal static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);  // WinBase.h

        [DllImport("Kernel32.dll", EntryPoint = "GetLastError", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Unicode)]
        extern static int GetLastError();

        [DllImport("Kernel32.dll")]
        static extern int QueryDosDevice(string lpDeviceName, IntPtr lpTargetPath, int ucchMax);

        [DllImport("Kernel32.dll", CharSet = CharSet.Ansi)]
        extern static IntPtr CreateFile(
            string strPath,
            uint dwDesiredAccess,
            uint dwShareMode,
            IntPtr lpSecurityAttributes,
            uint dwCreationDisposition,
            uint dwFlagsAndAttributes,
            IntPtr hTemplateFile);

        private static string[] QueryDosDevice()
        {
            // Allocate some memory to get a list of all system devices.
            // Start with a small size and dynamically give more space until we have enough room.
            int returnSize = 0;
            int maxSize = 100;
            string allDevices = null;
            IntPtr mem;
            string[] retval = null;

            while (returnSize == 0)
            {
                mem = Marshal.AllocHGlobal(maxSize);
                if (mem != IntPtr.Zero)
                {
                    // mem points to memory that needs freeing
                    try
                    {
                        returnSize = QueryDosDevice(null, mem, maxSize);
                        if (returnSize != 0)
                        {
                            allDevices = Marshal.PtrToStringAnsi(mem, returnSize);
                            retval = allDevices.Split('\0');
                            break;    // not really needed, but makes it more clear...
                        }
                        else if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
                        //maybe better
                        //else if( Marshal.GetLastWin32Error() == ERROR_INSUFFICIENT_BUFFER)
                        //ERROR_INSUFFICIENT_BUFFER = 122;
                        {
                            maxSize *= 10;
                        }
                        else
                        {
                            Marshal.ThrowExceptionForHR(GetLastError());
                        }
                    }
                    finally
                    {
                        Marshal.FreeHGlobal(mem);
                    }
                }
                else
                {
                    throw new OutOfMemoryException();
                }
            }
            return retval;
        }

        private void buttonReadFromDisk_Click(object sender, EventArgs e)
        {
            IntPtr hFile = CreateFile(@"\\.\"+ comboBox.SelectedItem.ToString(),
                GENERIC_READ,
                FILE_SHARE_READ | FILE_SHARE_WRITE,
                IntPtr.Zero,
                OPEN_EXISTING,
                FILE_ATTRIBUTE_NORMAL,
                IntPtr.Zero);
            if (hFile == INVALID_HANDLE_VALUE)
            {
                MessageBox.Show(GetLastError().ToString());
                return;
            }

            using (FileStream driveStream =
                new FileStream(new SafeFileHandle(hFile, true), FileAccess.Read))
            using (FileStream stream =
                new FileStream(""+comboBox.SelectedItem.ToString()+".bin", FileMode.OpenOrCreate, FileAccess.Write))
            {
                byte[] buffer = new byte[512];
                int readed = driveStream.Read(buffer, 0, 512);
                if (readed == 512)
                {
                    stream.Write(buffer, 0, readed);
                    mbr = buffer;
                }
            }
            FillInfo();
        }

        private void FillInfo()
        {
            listViewDetails.LabelWrap = true;
            listViewDetails.Items.Clear();
            int i = 446;
            int n = 0;
            while(i<512-2)
            {
                byte[] part = new byte[16];
                for (int m = 0; m < 16; part[m] = mbr[i + m++]);
                string size = (part[15].ToString("X2") + part[14].ToString("X2") + part[13].ToString("X2") + part[12].ToString("X2"));
                
                ListViewItem lv = new ListViewItem { Text = "Partition"+n};
                lv.SubItems.Add(part[0]==0x80 ? "Active" : "Not active");
                lv.SubItems.Add((Convert.ToInt64(size, 16) / 2048).ToString() + " MB"); // if sector contains 512 bytes
                lv.SubItems.Add(partype[part[4]].ToString());
                
                listViewDetails.Items.Add(lv);
                i += 16;
                n++;
            }
        }

        private void MDRForm_Load(object sender, EventArgs e)
        {
            foreach (var disk in QueryDosDevice())
            {
                if (disk.Length >= 13 && disk.Substring(0, 13).CompareTo("PhysicalDrive") == 0)
                    comboBox.Items.Add(disk);
            }
            if (comboBox.Items.Count > 0)
            {
                comboBox.SelectedIndex = 0;
            }

            partype = InitDictionary.Init();
        }


        private void buttonReadFromFile_Click(object sender, EventArgs e)
        {
            openFileDialog.DefaultExt = "bin";
            openFileDialog.Filter = "All Files (*.*)|*.*|Binary files (*.bin)|*.bin";
            openFileDialog.FilterIndex = 2;
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                using (BinaryReader stream = new BinaryReader(openFileDialog.OpenFile()))
                {
                    stream.Read(mbr, 0, 512);
                }
            }
            FillInfo();
        }
    }
}
