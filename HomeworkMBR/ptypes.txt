00  NO-Partition empty partition-table entry
01  DOS DOS 12-bit FAT
02 XENIX root file system
03 XENIX /usr file system (obsolete)
04 DOS 16-bit FAT (up to 32M)
05 Extended DOS 3.3+ extended partition
06 DOS 3.31+ Large File System (16-bit FAT, over 32M)
07  Advanced Unix, QNX QNX, OS/2 HPFS, WindowsNT NTFS
08 OS/2 (v1.0-1.3 only), AIX bootable partition, SplitDrive, Commodore Commodore DOS, DELL DELL partition spanning multiple drives
09 Coherent Coherent filesystem, AIX AIX data partition
0A OPUS OPUS, Coherent Coherent swap partition, OS/2 Boot Manager
0B Windows95 with 32-bit FAT
0C Windows95 with 32-bit FAT (using LBA-mode INT 13 extensions)
0E VFAT logical-block-addressable VFAT (same as 06h but using LBA)
0F Extended LBA Extended partition
10 OPUS OPUS
11 FAT12 OS/2 Boot Manager hidden 12-bit FAT partition
12 Compaq Compaq Diagnostics partition
14 FAT16 OS/2 Boot Manager hidden sub-32M 16-bit FAT partition
16 FAT16 OS/2 Boot Manager hidden over-32M 16-bit FAT partition
17 OS/2 OS/2 Boot Manager hidden HPFS partition, NTFS hidden NTFS partition
18 ASTSuspend AST special Windows swap file (�Zero-Volt Suspend� partition)
19 Willowtech Willowtech Photon coS
1B Windows hidden Windows95 FAT32 partition
1C Windows hidden Windows95 FAT32 partition (LBA-mode)
1E Windows hidden LBA VFAT partition
20 Willowsoft Willowsoft Overture File System (OFS1)
21 [reserved] officially listed as reserved, FSo2
23 [reserved] officially listed as reserved
24 NEC MS-DOS 3.x
26 [reserved] officially listed as reserved
31 [reserved] officially listed as reserved
33 [reserved] officially listed as reserved
34 [reserved] officially listed as reserved
36 [reserved] officially listed as reserved
38 Theos
3C PowerQuest PartitionMagic recovery partition
40 VENIX 80286
41 Personal RISC Boot, PowerPC boot partition
42 SFS(Secure File System) by Peter Gutmann
45 EUMEL/Elan
46 EUMEL/Elan
47 EUMEL/Elan
48 EUMEL/Elan
4F Oberon Oberon boot/data partition
50 OnTrack Disk Manager, read-only partition
51 OnTrack Disk Manager, read/write partition, NOVELL
52 CP/M, Microport System V/386
53 OnTrack Disk Manager, write-only partition???
54 OnTrack Disk Manager (DDO)
55 EZ-Drive EZ-Drive (see also INT 13/AH=FFh�EZ-Drive�)
56 GoldenBow GoldenBow VFeature
5C Priam Priam EDISK
61 SpeedStor
63 UnixSysV Unix SysV/386, 386/ix, Mach Mach, MtXinu BSD 4.3 on Mach, GNU-HURD GNU HURD
64 Novell Novell NetWare 286, SpeedStore SpeedStore
65 Novell NetWare (3.11)
67 Novell
68 Novell
69 Novell NSS Volume
70 DiskSecure DiskSecure Multi-Boot
71 [reserved] officially listed as reserved
73 [reserved] officially listed as reserved
74 [reserved] officially listed as reserved
75 PC/IX PC/IX
76 [reserved] officially listed as reserved
7E F.I.X. F.I.X.
80 Minix Minix v1.1 � 1.4a
81 Minix Minix v1.4b+, Linux Linux, Mitac Mitac Advanced Disk Manager
82 Linux/Swap Linux Swap partition, Prime Prime, Solaris Solaris (Unix)
83 Linux Linux native file system (ext2fs/xiafs)
84 DOS OS/2-renumbered type 04h partition (hiding DOS C: drive)
85 Linux Linux EXT
86 FAT16 FAT16 volume/stripe set (Windows NT)
87 HPFS HPFS Fault-Tolerant mirrored partition, NTFS NTFS volume/stripe set
93 Amoeba Amoeba file system
94 Amoeba Amoeba bad block table
98 Datalight Datalight ROM-DOS SuperBoot
99 Mylex Mylex EISA SCSI
A0 Phoenix Phoenix NoteBIOS Power Management �Save-to-Disk� partition
A1 [reserved] officially listed as reserved
A3 [reserved] officially listed as reserved
A4 [reserved] officially listed as reserved
A5 FreeBSD FreeBSD, BSD/386
A6 OpenBSD OpenBSD
A9 NetBSD NetBSD (http://www.netbsd.org/)
B1 [reserved] officially listed as reserved
B3 [reserved] officially listed as reserved
B4 [reserved] officially listed as reserved
B6 [reserved] officially listed as reserved, Windows Windows NT mirror set (master), FAT16 file system
B7 BSDI BSDI file system (secondarily swap), Windows Windows NT mirror set (master), NTFS file system
B8 BSDI BSDI swap partition (secondarily file system)
BE Solaris Solaris boot partition
C0 CTOS CTOS, DR-DOS DR DOS/DR-DOS/Novell DOS secured partition
C1 DR-DOS6.0 DR DOS 6.0 LOGIN.EXE-secured 12-bit FAT partition
C4 DR-DOS6.0 DR DOS 6.0 LOGIN.EXE-secured 16-bit FAT partition
C6 DR-DOS6.0 DR DOS 6.0 LOGIN.EXE-secured Huge partition, corrupted corrupted FAT16 volume/stripe set (Windows NT), Windows Windows NT mirror set (slave), FAT16 file system
C7 Syrinx Syrinx Boot, corrupted corrupted NTFS volume/stripe set, Windows Windows NT mirror set (slave), NTFS file system
CB DR-DOS Reserved for DR DOS/DR-DOS/OpenDOS secured FAT32
CC DR-DOS Reserved for DR DOS/DR-DOS secured FAT32 (LBA)
CE DR-DOS Reserved for DR DOS/DR-DOS secured FAT16 (LBA)
D0 Multiuser Multiuser DOS secured FAT12
D1 Old-FAT12 Old Multiuser DOS secured FAT12
D4 Old-FAT16 Old Multiuser DOS secured FAT16 (<= 32M)
D5 Old-Ext Old Multiuser DOS secured extended partition
D6 Old-FAT16 Old Multiuser DOS secured FAT16 (> 32M)
D8 CP/M-86 CP/M-86, CP/M CP/M, Concurrent CP/M, Concurrent DOS, CTOS CTOS (Convergent Technologies OS)
E1 SpeedStor SpeedStor 12-bit FAT extended partition
E2 DOS DOS read-only (Florian Painke�s XFDISK 1.0.4)
E3 DOS DOS read-only, Storage Storage Dimensions
E4 SpeedStor SpeedStor 16-bit FAT extended partition
E5 [reserved] officially listed as reserved
E6 [reserved] officially listed as reserved
EB BeOS BeOS BFS (BFS1)
F1 Storage Storage Dimensions
F2 DOS DOS 3.3+ secondary partition
F3 [reserved] officially listed as reserved
F4 SpeedStor SpeedStor, Storage Storage Dimensions
F5 Prologue Prologue
F6 [reserved] officially listed as reserved
FB VMWARE vmware partition
FE LANstep LANstep, PS/2-IML IBM PS/2 IML (Initial Microcode Load) partition
FF Xenix Xenix bad block table